class Channel < ApplicationRecord
  has_many :messages, dependent: :destroy
  has_many :users, through: :messages
  has_many :user_channelships, dependent: :destroy

  validates :topic, presence: true, uniqueness: true

  before_validation :topic_downcase

  protected

  def topic_downcase
    self.topic.downcase!
  end
end
