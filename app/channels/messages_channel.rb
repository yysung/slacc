# Be sure to restart your server when you modify this file. Action Cable runs in a loop that does not support auto reloading.
class MessagesChannel < ApplicationCable::Channel
  include ActionView::Helpers::AssetTagHelper
  def subscribed
    stream_from "messages"
  end

  def unsubscribed
    # Any cleanup needed when channel is unsubscribed
  end

  def receive(payload)
    message = Message.create(user: current_user, channel_id: payload["channel_id"], content: payload["message"])
    ActionCable.server.broadcast('messages', {message: message.content.gsub(/\n/, '<br>'), channel_id: message.channel_id,
      username: current_user.username, time: message.created_at.strftime("%I:%M %p")})
    uc = current_user.user_channelships.find_by(channel_id: payload["channel_id"])
    uc.try(:update, {:read => message.id})
  end

  def read_update(payload)
    uc = current_user.user_channelships.find_by(channel_id: payload["channel_id"])
    uc.try(:update, {:read => Channel.find_by(id: payload["channel_id"]).messages.last.id})
  end
end
